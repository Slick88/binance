import * as React from "react";
import "./styles.css";
import { Container } from "react-bootstrap";
import Header from './Header'
import CenterRow from "./CenterRow";
import Footer from './Footer'

export default function App() {
  return (
    <Container fluid className={"main-app"}>
      <Header/>
      <CenterRow />
      <Footer/>
    </Container>
  );
}
