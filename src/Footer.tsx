import * as React from "react";
import {Col, Row, Button} from "react-bootstrap";
import './App.scss'

export default () => {
    return (
        <Row>
            <Col className={'footer'}>
                <div className={'button-container'}>
                    <Button className={'click-button'} variant="primary">
                        Click here!
                    </Button>
                </div>
            </Col>
        </Row>
    )
}