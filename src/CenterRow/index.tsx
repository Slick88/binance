import * as React from "react";
import "./CenterRow.scss";
import {Row} from "react-bootstrap";
import LeftSection from "./LeftSection";
import CenterSection from "./CenterSection";
import RightSection from "./RightSection";

export default () => {
  return (
    <Row className={"center-section-main-row"}>
      <LeftSection />
      <CenterSection />
      <RightSection />
    </Row>
  );
};
