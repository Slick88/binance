import * as React from "react";
import "./CenterRow.scss";
import { Row, Col } from "react-bootstrap";

export default () => {
  return (
    <Col xl={{order: 1}} xs={{span:12, order: 2}}>
      <Row className={"left-section-top-row"}>
        <Col className={"left-section-top-col"} xl={12} xs={6}/>
        <Col className={'left-section-bottom-col'} xl={12} xs={6}/>
      </Row>
    </Col>
  );
};
